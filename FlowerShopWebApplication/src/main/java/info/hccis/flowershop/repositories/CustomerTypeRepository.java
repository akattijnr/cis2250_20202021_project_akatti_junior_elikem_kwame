package info.hccis.flowershop.repositories;

import info.hccis.flowershop.entity.jpa.CustomerType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerTypeRepository extends CrudRepository<CustomerType, Integer> {
}