/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.flowershop.controllers;

//import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import info.hccis.flowershop.bo.FlowerOrderBO;
import info.hccis.flowershop.dao.FlowerOrderDAO;
import info.hccis.flowershop.entity.jpa.FlowerOrder;
import info.hccis.flowershop.repositories.FlowerOrderRepository;
import info.hccis.flowershop.util.CisUtility;
import info.hccis.flowershop.util.FlowerOrderFileUtility;
import java.io.File;
import static java.lang.String.format;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for the Order functionality of the
 * flower shop application
 * 
 * @author Logan MacKinnon
 * @since 20201014
 */
@Controller
@RequestMapping("/orders")
public class OrderController {
    
    private final FlowerOrderRepository orderRepository;
    
    public OrderController(FlowerOrderRepository oR){
        this.orderRepository = oR;
    }
    
    /**
     * Page to allow user to view current orders
     *
     * @since 20201013
     * @author LRM (modified from BJ's project)
     */
    @RequestMapping("/list")
    public String list(Model model) {

        //Retrieve all orders from db
        FlowerOrderBO orderBO = new FlowerOrderBO();
        ArrayList<FlowerOrder> orders = (ArrayList<FlowerOrder>) orderRepository.findAll(); //orderBO.selectAllOrders();
        
        //Get customer names
        HashMap<Integer, String> customers = orderBO.getCustomerNames();
        
        //Get Order Status
        HashMap<Integer, String> statuses = orderBO.getOrderStatuses();

        model.addAttribute("orders", orders);
        model.addAttribute("customers", customers);
        model.addAttribute("statuses", statuses);

        return "orders/list";
    }
    
    /**
     * Page to allow user make new order
     *
     * @param model
     * @return 
     * @since 20201013
     * @author LRM (modified from BJ's project)
     */
    @RequestMapping("/add")
    public String add(Model model) {

        //New FlowerOrder object
        FlowerOrder order = new FlowerOrder();
        order.setId(0);
        
        //New FlowerOrderBO
        FlowerOrderBO orderBO = new FlowerOrderBO();
        //Customer names from database
        HashMap<Integer, String> customers = orderBO.getCustomerNames();
        //Flower names, ids from database
        HashMap<Integer, String> flowers = orderBO.getFlowers();
        //Order statusesm ids
        HashMap<Integer, String> statuses = orderBO.getOrderStatuses();
        
        model.addAttribute("order", order);
        model.addAttribute("customers", customers);
        model.addAttribute("flowers", flowers);
        model.addAttribute("statuses", statuses);

        return "orders/add";
    }
    
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model, @Valid @ModelAttribute("order") FlowerOrder order, BindingResult result){
        
        if(result.hasErrors()){
            System.out.println("Validation error");
            return "orders/add";
        }
        //Insert new order to database
        FlowerOrderBO orderBO = new FlowerOrderBO();
        //Set order date
        order.setOrderDate(CisUtility.getCurrentDate("yyyy-MM-dd"));
        //Set order cost
        order.setTotalCost(orderBO.totalCost(order));
        
        //Add new order to database
        orderRepository.save(order);
        
        model.addAttribute("orders", orderRepository.findAll());
        model.addAttribute("customers", orderBO.getCustomerNames());
        model.addAttribute("statuses", orderBO.getOrderStatuses());
        
        return "orders/list";
    }
    
    /**
     * Page to allow user to edit a parking pass's information
     *
     * @since 20201007
     * @author LRM (modified from BJ's camper project)
     */
    @RequestMapping("/edit")
    public String edit(Model model, HttpServletRequest request) {

        FlowerOrderBO orderBO = new FlowerOrderBO();
        
        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("LoganTEST - id=" + id);

        Optional<FlowerOrder> found = orderRepository.findById(id);

        model.addAttribute("order", found);
        model.addAttribute("customers", orderBO.getCustomerNames());
        model.addAttribute("flowers", orderBO.getFlowers());
        model.addAttribute("statuses", orderBO.getOrderStatuses());
        
        return "orders/add";
    }
    
    /**
     * Allows user to delete a specific order from the table
     * 
     * @since 20201023
     * @author LRM (modified from BJ's camper project)
     * 
     * @param model
     * @param request
     * @return 
     */
    @RequestMapping("/delete")
    public String delete(Model model, HttpServletRequest request){
        
        FlowerOrderBO orderBO = new FlowerOrderBO();
        
        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("LoganTEST - id=" + id);
        
        String success = "";
        
        try{
            orderRepository.deleteById(id);
            success = "Order deleted successully: (" + id + ")";
        }catch(EmptyResultDataAccessException e){
            success = "Order not deleted. Try again later.";
        }
        
        model.addAttribute("message", success);
        model.addAttribute("orders", orderRepository.findAll());
        model.addAttribute("customers", orderBO.getCustomerNames());
        model.addAttribute("statuses", orderBO.getOrderStatuses());
        
        return "orders/list";
    }
    
    /**
     * Page to allow user to find an order
     * between specified dates
     * 
     * @author LRM (modified from BJs camper project)
     * @since 20201028
     * 
     * @param model
     * @return 
     */    
    @RequestMapping("/find")
    public String find(Model model){
        String message = "Enter a start date and end date to generate report!";
        model.addAttribute("message", message);
        return "orders/find";
    }
    
    /**
     * Page to allow user to see orders between
     * two specified dates
     * 
     * @author LRM (modified from BJs camper project)
     * @since 20201028
     * 
     * @param model
     * @param order
     * @return 
     */
    @RequestMapping("/findSubmit")
    public String dateSubmit(Model model, @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate){
        FlowerOrderBO orderBO = new FlowerOrderBO();
        
        ArrayList<FlowerOrder> orderReport = loadOrdersByDates(startDate, endDate);
        
        model.addAttribute("orders", orderReport);
        model.addAttribute("customers", orderBO.getCustomerNames());
        model.addAttribute("statuses", orderBO.getOrderStatuses());
        model.addAttribute("reportTotal", "Total cost for Report: $" + String.format("%.2f", getReportTotal(orderReport)));
        return "orders/list";
    }
    
    /**
     * Allow user to export current orders table to a JSON file
     * for a report
     * 
     * @author LRM
     * @since 20201029
     * 
     * @param model
     * @return 
     */
    @RequestMapping("/export")
    public String export(Model model){     
        
        //Retrieve all orders from db
        FlowerOrderBO orderBO = new FlowerOrderBO();
        ArrayList<FlowerOrder> orders = (ArrayList<FlowerOrder>) orderRepository.findAll();
        
        //Declare file path
        String filePath = "C:\\flowers\\order_" + CisUtility.getCurrentDate("yyyyMMddhhmmss") + ".json";
        
        //Create file
        File file = FlowerOrderFileUtility.setupFile(filePath);
        
        //Set status for user to know if file written
        String statusMessage = FlowerOrderFileUtility.write(orders, file) ? "File successfully written to " + file.getPath() 
                + "." : "Something went wrong. Try again later.";
        
        //Get customer names
        HashMap<Integer, String> customers = orderBO.getCustomerNames();
        
        //Get Order Status
        HashMap<Integer, String> statuses = orderBO.getOrderStatuses();

        model.addAttribute("orders", orders);
        model.addAttribute("customers", customers);
        model.addAttribute("statuses", statuses);
        model.addAttribute("message", statusMessage);
        
        return "orders/list";
    }
    
    /**
     * Generate a report by specifying start date and
     * end date of report
     * 
     * @author LRM
     * @since 20201029
     * 
     * @param startDate
     * @param endDate
     * @return 
     */
    public ArrayList<FlowerOrder> loadOrdersByDates(String startDate, String endDate){
        ArrayList<FlowerOrder> orders = (ArrayList<FlowerOrder>) orderRepository.findAll();
        ArrayList<FlowerOrder> newList = new ArrayList();
        try {
            //Convert date strings to date objects for comparison
            Date start = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
            Date end = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);

            //Loop through all orders for orders within date range specified
            for(FlowerOrder current : orders){
                Date currentWorkingDate = new SimpleDateFormat("yyyy-MM-dd").parse(current.getOrderDate());
                System.out.println("Current: " + currentWorkingDate);
                if(currentWorkingDate.equals(start) || currentWorkingDate.equals(end)){
                    newList.add(current);
                }else if(currentWorkingDate.after(start) && currentWorkingDate.before(end)){
                    newList.add(current);
                }
            }
        } catch (ParseException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return newList;
    }
    
    /**
     * Generate total cost of generated report
     * 
     * @author LRM
     * @since 20201029
     * 
     * @param reportOrders
     * @return 
     */
    public BigDecimal getReportTotal(ArrayList<FlowerOrder> reportOrders){
        
        BigDecimal runningTotal = new BigDecimal(0);
        
        for(FlowerOrder current : reportOrders){
            runningTotal = runningTotal.add(current.getTotalCost());
        }
        
        return runningTotal;
    }
    
    
}
