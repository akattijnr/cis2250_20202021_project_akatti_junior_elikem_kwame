package info.hccis.flowershop.dao;

import info.hccis.flowershop.entity.jpa.Customer;
import info.hccis.flowershop.entity.jpa.CustomerType;
import info.hccis.flowershop.util.CisUtility;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Customer DAO class for accessing customers from the database.
 *
 * @author Parker
 * @since 2020-10-22
 */
public class CustomerDAO
{
    private String userName=null, password=null, connectionString=null;
    private Connection conn = null;
    
    public CustomerDAO()
    {
        String propFileName = "application";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        System.out.println("bjtest datasource:  " + rb.getString("spring.datasource.url"));
        connectionString = rb.getString("spring.datasource.url");
        userName = rb.getString("spring.datasource.username");
        password = rb.getString("spring.datasource.password");

        try {
            conn = DriverManager.getConnection(
                    connectionString, userName, password);
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Insert a customer into the database.
     * @since 2020-10-22
     * @author PAAG
     */
    public void insert(Customer customer) 
    {

        //***************************************************
        // INSERT
        //***************************************************
        try {
            String theStatement = "INSERT INTO Customer(id, customerTypeId, fullName, address1, "
                    + "city, province, postalCode, phoneNumber, birthDate, loyaltyCard) "
                    + "VALUES (0,?,?,?,?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, customer.getCustomerTypeId());
            stmt.setString(2, customer.getFullName());
            stmt.setString(3, customer.getAddress1());
            stmt.setString(4, customer.getCity());
            stmt.setString(5, customer.getProvince());
            stmt.setString(6, customer.getPostalCode());
            stmt.setString(7, customer.getPhoneNumber());
            stmt.setString(8, customer.getBirthDate());
            stmt.setString(9, customer.getLoyaltyCard());

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }
    
    /**
     * Update a customer in the database.
     * @since 2020-10-22
     * @author PAAG
     */
    public void update(Customer customer) {

        //***************************************************
        // INSERT
        //***************************************************
        try {
            String theStatement = "UPDATE Customer SET customerTypeId=?,fullName=?,address1=?,city=?,province=?,postalCode=?,phoneNumber=?,birthDate=?,loyaltyCard=? WHERE id=?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, customer.getCustomerTypeId());
            stmt.setString(2, customer.getFullName());
            stmt.setString(3, customer.getAddress1());
            stmt.setString(4, customer.getCity());
            stmt.setString(5, customer.getProvince());
            stmt.setString(6, customer.getPostalCode());
            stmt.setString(7, customer.getPhoneNumber());
            stmt.setString(8, customer.getBirthDate());
            stmt.setString(9, customer.getLoyaltyCard());
            stmt.setInt(10, customer.getId());

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }
    
    /**
     * Delete a customer from the database. 
     * @since 2020-10-22
     * @author PAAG
     * 
     */
    public void delete(int id) {

        //***************************************************
        // INSERT
        //***************************************************
        try {
            String theStatement = "DELETE FROM customer WHERE id=?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, id);
            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }
        
    }
    
    /**
     * Select all customers from the database
     * @since 2020-05-21
     * @author BJM
     */
    public ArrayList<Customer> selectAll() {
        ArrayList<Customer> customers = new ArrayList();
        ArrayList<CustomerType> customerTypes = new ArrayList();
        CustomerTypeDAO customerTypeDAO = new CustomerTypeDAO();
        customerTypes = customerTypeDAO.select();
        
        try {
            Statement stmt = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = stmt.executeQuery("select * from Customer");

            //Show all the bookers
            while (rs.next()) {

                int id = rs.getInt("id");
                int customerTypeId = rs.getInt("customerTypeId");
                String fullName = rs.getString("fullName");
                String address1 = rs.getString("address1");
                String city = rs.getString("city");
                String province = rs.getString("province");
                String postalCode = rs.getString("postalCode");
                String phoneNumber = rs.getString("phoneNumber");
                String birthDate = rs.getString("birthDate");
                String loyaltyCard = rs.getString("loyaltyCard");
                
                
                Customer customer = new Customer(id);
                customer.setCustomerTypeId(customerTypeId);
                customer.setFullName(fullName);
                customer.setAddress1(address1);
                customer.setCity(city);
                customer.setProvince(province);
                customer.setPostalCode(postalCode);
                customer.setPhoneNumber(phoneNumber);
                customer.setBirthDate(birthDate);
                customer.setLoyaltyCard(loyaltyCard);
                
                for(CustomerType current: customerTypes){
                    if(current.getId().equals(customerTypeId) ){
                        customer.setCustomerTypeDescription(current.getDescription());
                    }
                }
                
                customers.add(customer);
                System.out.println("Customer for id: " + rs.getString("id") + " is " + rs.getString("fullName"));
                System.out.println("Customer type description: " + customer.getCustomerTypeDescription());
            }
        } catch (SQLException ex) {
            System.out.println("Error selecting customers. (" + ex.getMessage() + ")");

        }
        return customers;

    }
    
    /**
     * Select a customer from the database
     * @since 2020-10-22
     * @author PAAG
     */
    public Customer select(int id) {
        ArrayList<Customer> customers = new ArrayList();
        
        ArrayList<CustomerType> customerTypes = new ArrayList();
        CustomerTypeDAO customerTypeDAO = new CustomerTypeDAO();
        customerTypes = customerTypeDAO.select();
        
        Customer customer = null;

        try {
            Statement stmt = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = stmt.executeQuery("select * from Customer where id="+id);
            
            //Show all the bookers
            while (rs.next()) {

                int customerTypeId = rs.getInt("customerTypeId");
                String fullName = rs.getString("fullName");
                String address1 = rs.getString("address1");
                String city = rs.getString("city");
                String province = rs.getString("province");
                String postalCode = rs.getString("postalCode");
                String phoneNumber = rs.getString("phoneNumber");
                String birthDate = rs.getString("birthDate");
                String loyaltyCard = rs.getString("loyaltyCard");
                
                customer = new Customer(id);
                customer.setCustomerTypeId(customerTypeId);
                customer.setFullName(fullName);
                customer.setAddress1(address1);
                customer.setCity(city);
                customer.setProvince(province);
                customer.setPostalCode(postalCode);
                customer.setPhoneNumber(phoneNumber);
                customer.setBirthDate(birthDate);
                customer.setLoyaltyCard(loyaltyCard);

                for(CustomerType current: customerTypes){
                    if(current.getId().equals(customerTypeId) ){
                        customer.setCustomerTypeDescription(current.getDescription());
                    }
                }
                
                customers.add(customer);
                System.out.println("Customer for id: " + rs.getString("id") + " is " + rs.getString("fullName"));
            }
        } catch (SQLException ex) {
            System.out.println("Error selecting bookings. (" + ex.getMessage() + ")");
        }
        return customer;

    }
}
