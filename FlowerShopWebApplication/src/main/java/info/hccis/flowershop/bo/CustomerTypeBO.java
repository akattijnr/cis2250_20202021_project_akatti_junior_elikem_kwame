package info.hccis.flowershop.bo;

import info.hccis.flowershop.dao.CustomerTypeDAO;
import info.hccis.flowershop.entity.jpa.CustomerType;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Parker
 */
public class CustomerTypeBO
{
    private static HashMap<Integer, String> customerTypesMap = new HashMap();

    public static HashMap<Integer, String> getCustomerTypesMap() {
        return customerTypesMap;
    }

    public static void setCustomerTypesMap(HashMap<Integer, String> customerTypesMap) {
        customerTypesMap = customerTypesMap;
    }

    
    
    
    
    /**
     * Connect to the data access object to get the campers from the datasource.
     *
     * @since 20200923
     * @author BJM
     */
    public ArrayList<CustomerType> load() {

        //Read from the database
        CustomerTypeDAO customerTypeDAO = new CustomerTypeDAO();
        ArrayList<CustomerType> customerTypes = customerTypeDAO.select();

        
        
        return customerTypes;
    }
}
