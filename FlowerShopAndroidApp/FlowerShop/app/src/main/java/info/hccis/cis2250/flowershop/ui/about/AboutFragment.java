package info.hccis.cis2250.flowershop.ui.about;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import info.hccis.cis2250.flowershop.R;

public class AboutFragment extends Fragment {

    private AboutViewModel aboutViewModel;
    //private View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        aboutViewModel =
                new ViewModelProvider(this).get(AboutViewModel.class);
        View root = inflater.inflate(R.layout.fragment_about, container, false);
        //final TextView textView = root.findViewById(R.id.text_feedback);
        aboutViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        Button btnShare = root.findViewById(R.id.button_share);
        TextView tvDevName = root.findViewById(R.id.textview_developer_name);
        TextView tvDevEmail = root.findViewById(R.id.text_view_email_input);
        TextView tvDevManagerName = root.findViewById(R.id.text_view_supervising_manager_name);

        /*
         * Set on click listener when click on share the contact info
         * Date: 2021/04/14
         * Purpose: used to share info via email and social media
         */
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String shareEmail = "Email here";
                String shareSub = "Subject here";
                String shareBody = "Body here";
                intent.putExtra(Intent.EXTRA_EMAIL,  shareEmail);
                intent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
                intent.putExtra(Intent.EXTRA_TEXT, (tvDevName.getText().toString()+ System.getProperty("line.separator")
                        +tvDevManagerName.getText().toString()+ System.getProperty("line.separator")+
                        tvDevEmail.getText().toString()+ System.getProperty("line.separator")));
                startActivity(Intent.createChooser(intent, "Share using"));
            }
        });

        return root;
    }
}