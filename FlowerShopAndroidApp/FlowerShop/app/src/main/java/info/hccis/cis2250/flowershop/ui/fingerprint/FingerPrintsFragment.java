package info.hccis.cis2250.flowershop.ui.fingerprint;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import java.util.concurrent.Executor;

import info.hccis.cis2250.flowershop.MainActivity;
import info.hccis.cis2250.flowershop.R;

public class FingerPrintsFragment extends Fragment {

    private GalleryViewModel galleryViewModel;
    private Executor executor;
    private androidx.biometric.BiometricPrompt biometricPrompt;
    private androidx.biometric.BiometricPrompt.PromptInfo promptInfo;

    private TextView tvFingerPrint;
    private Button btnFingerPrint;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                new ViewModelProvider(this).get(GalleryViewModel.class);
        View root = inflater.inflate(R.layout.fragment_finger_prints, container, false);

        galleryViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
               // textView.setText(s);

            }

        });

        executor = ContextCompat.getMainExecutor(getActivity());


        biometricPrompt = new BiometricPrompt(getActivity(), executor, new androidx.biometric.BiometricPrompt.AuthenticationCallback() {

            //Method when there is an error
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString){
                super.onAuthenticationError(errorCode, errString);
                //tvFingerPrint.setText("Authentication error: "+errString);
                Toast.makeText(getActivity(), "Authentication error: " + errString, Toast.LENGTH_LONG).show();
            }

            //Method when it is successful
            @Override
            public void onAuthenticationSucceeded(@NonNull androidx.biometric.BiometricPrompt.AuthenticationResult result){
                super.onAuthenticationSucceeded(result);
                //Send them to the homepage if success.
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                Toast.makeText(getActivity(), "AAuthentication succed...! ", Toast.LENGTH_LONG).show();
            }

            //Method when authentification fail
            @Override
            public void onAuthenticationFailed(){
                super.onAuthenticationFailed();
                //tvFingerPrint.setText("Authentication failed...!");
                Toast.makeText(getActivity(), "AAuthentication failed...! ", Toast.LENGTH_LONG).show();
            }
        });

        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Biometric Authentication")
                .setSubtitle("Login using fingerprint authentication")
                .setNegativeButtonText("User App Password")
                .build();


        btnFingerPrint = root.findViewById(R.id.button_finger_print);

        btnFingerPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                biometricPrompt.authenticate(promptInfo);
            }
        });





        return root;
    }
}