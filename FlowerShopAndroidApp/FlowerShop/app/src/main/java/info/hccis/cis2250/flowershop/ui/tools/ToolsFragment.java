package info.hccis.cis2250.flowershop.ui.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import info.hccis.cis2250.flowershop.R;
import info.hccis.cis2250.flowershop.ui.customers.CustomersFragment;


public class ToolsFragment extends Fragment {

    private ToolsViewModel toolsViewModel;

    private Button buttonSend;
    private ToggleButton toggleButtonRoom;

   // private OnFragmentInteractionListener mListener;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        toolsViewModel =
                ViewModelProviders.of(this).get(ToolsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_tools, container, false);

        //BJM 20200207 Adding code to handle toggle button
        toggleButtonRoom = root.findViewById(R.id.toggleButtonLoadFromRoom);

        boolean loadFromRoom;
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        loadFromRoom = sharedPref.getBoolean(getString(R.string.preference_load_from_room), false);
        toggleButtonRoom.setChecked(loadFromRoom);

        //Reference:  https://developer.android.com/guide/topics/ui/controls/togglebutton
        toggleButtonRoom.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(getString(R.string.preference_load_from_room), isChecked);
                editor.commit();
                Log.d("bjm setting preference", "set to " + isChecked);
            }
        });


        return root;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
