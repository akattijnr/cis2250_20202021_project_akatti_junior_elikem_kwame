package info.hccis.cis2250.flowershop.ui.gallery;

import android.os.Bundle;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import info.hccis.cis2250.flowershop.R;

public class FullViewActivity extends AppCompatActivity {

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_view);

        imageView = findViewById(R.id.img_full);

        int img_id = getIntent().getExtras().getInt("img_id");
        imageView.setImageResource(img_id);

    }
}