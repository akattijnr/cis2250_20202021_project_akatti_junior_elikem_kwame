package info.hccis.cis2250.flowershop.ui.gallery;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import info.hccis.cis2250.flowershop.R;


public class GalleryFragment extends Fragment {

    //https://www.youtube.com/watch?v=L8VlPi8mq-g

    private TextView image_name;
    private ImageView image;
    private Button btn_full;
    private Button btnClose;

    //List for images to be used for gallery
    ArrayList<Integer> mImageIds = new ArrayList<>(Arrays.asList(
       R.drawable.image_1, R.drawable.image_2,R.drawable.image_3, R.drawable.image_4, R.drawable.image_5,R.drawable.image_6,
       R.drawable.image_7, R.drawable.image_8, R.drawable.image_9, R.drawable.image_10, R.drawable.image_11
    ));

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_gallery, container, false);


        GridView gridView = root.findViewById(R.id.myGrid);
        gridView.setAdapter(new ImageAdapter(mImageIds, getActivity()));


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int item_pos = mImageIds.get(position);

               // ShowDialogBox(item_pos);
            }
        });

        return root;
    }


    public void ShowDialogBox(int item_pos){
        Dialog dialog = new Dialog(getActivity());

        dialog.setContentView(R.layout.fragment_custom_dialog);

        image_name = dialog.findViewById(R.id.txt_image_name);
        image = dialog.findViewById(R.id.image);
        btn_full = dialog.findViewById(R.id.btn_full);
        btnClose = dialog.findViewById(R.id.btn_close);

        String title = getResources().getResourceName(item_pos);

        int index = title.indexOf("/");
        String name = title.substring(index+1, title.length());

        image_name.setText(name);
        image.setImageResource(item_pos);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_full.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), FullViewActivity.class);
                i.putExtra("img_id", item_pos);
                startActivity(i);
            }
        });

        dialog.show();
    }
}