package info.hccis.cis2250.flowershop.ui.customerdetail;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import info.hccis.cis2250.flowershop.R;
import info.hccis.cis2250.flowershop.bo.Customer;

public class CustomerDetailsFragment extends Fragment {

    private Customer customer;
    private TextView tvFName;
    private TextView tvAddress;
    private TextView tvCity;
    private TextView tvProvince;
    private TextView tvPostalCode;
    private TextView tvPhoneNumber;
    private TextView  tvBirthDate;
    private TextView tvLoyaltyCard;

    private Button btnSaveCustomer;


    public static CustomerDetailsFragment newInstance()  { return new CustomerDetailsFragment(); }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         /*
          BJM 20200202
          Note how the arguments are passed from the activity to this fragment.  The
          camper object that was chosen on the recyclerview was encoded as a json string and then
          decoded when this fragment is accessed.
         */

        if(getArguments() != null) {
            String customerJson = getArguments().getString("customer");
            Gson gson = new Gson();
            customer = gson.fromJson(customerJson, Customer.class);
        }

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_customer_details, container, false);

        tvFName = view.findViewById(R.id.textview_detail_full_name);
        tvAddress = view.findViewById(R.id.textview_detail_address);
        tvBirthDate = view.findViewById(R.id.textview_detail_birth_date);
        tvCity = view.findViewById(R.id.textview_detail_city);
        tvProvince = view.findViewById(R.id.textview_detail_province);
        tvPostalCode = view.findViewById(R.id.textview_detail_postal_code);
        tvPhoneNumber = view.findViewById(R.id.textview_detail_phone_number);
        tvLoyaltyCard = view.findViewById(R.id.textview_detail_loyalty_card);


        tvFName.setText(customer.getFullName());
        tvAddress.setText(customer.getAddress1());
        tvBirthDate.setText(customer.getBirthDate());
        tvCity.setText(customer.getCity());
        tvProvince.setText(customer.getProvince());
        tvPostalCode.setText(customer.getPostalCode());
        tvPhoneNumber.setText(customer.getPhoneNumber());
        tvLoyaltyCard.setText(customer.getLoyaltyCard());

        btnSaveCustomer = view.findViewById(R.id.btn_save_customer);

        /**
         * Clicking this button will provoke the add contact method.
         * @author Elikem Akatti Junior
         * @since 20210412
         */
        btnSaveCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addContact();
            }
        });

        return view;
    }


    /**
     * Contact method when invoked creates an intent and and save the contacts
     * @author Elikem Akatti Junior
     * @since 20210412
     */
    public void addContact() {
        Intent saveContactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
        saveContactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

        // Taking the information from textfields/radio buttons and applying them to the intent.
        saveContactIntent.putExtra(ContactsContract.Intents.Insert.NAME, tvFName.getText().toString());
        saveContactIntent.putExtra(ContactsContract.Intents.Insert.POSTAL, tvPostalCode.getText().toString());
        saveContactIntent.putExtra(ContactsContract.Intents.Insert.PHONE, tvPhoneNumber.getText().toString());

        //Start the activity
        startActivity(saveContactIntent);
    }
}
