package info.hccis.cis2250.flowershop.ui.customers;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import info.hccis.cis2250.flowershop.MainActivity;
import info.hccis.cis2250.flowershop.R;
import info.hccis.cis2250.flowershop.bo.Customer;
import info.hccis.cis2250.flowershop.bo.CustomerContentVolley;
import info.hccis.cis2250.flowershop.util.Util;

public class CustomerAddFragment  extends Fragment  {

    //Creating global variables
    private Customer customer;
    private EditText edFName;
    private EditText edAddress;
    private EditText edCity;
    private EditText edProvince;
    private EditText edPostalCode;
    private EditText edPhone;
    private EditText edBOD;
    private EditText edLoyaltyCard;

    private Button btnAddCustomer;


    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("EAKJ","Oncreate clicked.");

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_customer_add, container, false);

        edFName = view.findViewById(R.id.editTextName);
        edAddress = view.findViewById(R.id.editTextAddress);
        edCity = view.findViewById(R.id.editTextCity);
        edProvince = view.findViewById(R.id.editTextProvince);
        edPostalCode = view.findViewById(R.id.editTextPostalCode);
        edPhone = view.findViewById(R.id.editTextPhone);
        edBOD = view.findViewById(R.id.editTextDOB);
        edLoyaltyCard = view.findViewById(R.id.editTextLoyaltyCard);

        btnAddCustomer = view.findViewById(R.id.buttonAddCustomer);


        /*
        EAKJ 20200202
        Onclick listener event that validates and saves the customer information
        */
        btnAddCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Validating to make sure that no space is left blank
                if(edFName.getText().toString().isEmpty()){
                    edFName.setError("Please enter your name.");
                }else if(edAddress.getText().toString().isEmpty()){
                    edAddress.setError("Please enter your textview_detail_address.");
                }else if(edCity.getText().toString().isEmpty()){
                    edCity.setError("Please enter your city.");
                }else if (edProvince.getText().toString().isEmpty()){
                    edProvince.setError("Please enter your province.");
                }else if (edPostalCode.getText().toString().isEmpty()){
                    edPostalCode.setError("Please enter your postal code.");
                }else if (edPhone.getText().toString().isEmpty()){
                    edPhone.setError("Please enter your phone.");
                }else if (edBOD.getText().toString().isEmpty()){
                    edBOD.setError("Please enter your birth date.");
                }else{

                    //If all input are entered, create a Gson object and cstore the input
                    Gson gson = new Gson();

                    //Creating customer object
                    Customer customer = new Customer();

                    //Setting texts for customer object details
                    customer.setFullName(edFName.getText().toString());
                    customer.setAddress1(edAddress.getText().toString());
                    customer.setCity(edCity.getText().toString());
                    customer.setProvince(edProvince.getText().toString());
                    customer.setPostalCode(edPostalCode.getText().toString());
                    customer.setPhoneNumber(edPhone.getText().toString());
                    customer.setBirthDate(edBOD.getText().toString());
                    customer.setLoyaltyCard(edLoyaltyCard.getText().toString());

                    //Creating a Json string and converting customer object to it.
                    String customerJson = gson.toJson(customer);


                    Log.d("EAKJ", "button clicked.  " + customerJson);

                    //Creating a Customer content volley object
                    CustomerContentVolley ccv = new CustomerContentVolley();
                    ccv.makeRequestPost(getContext(), Util.CUSTOMER_BASE_API + "customers", customerJson);

                    //Sure success message
                    Toast toast = Toast.makeText(getContext(), "Customer added successfully.", Toast.LENGTH_LONG);
                    toast.show();

                    //When all goes well, send them back to the home page.
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);

                }
            }
        });
        return view;
    }
}
