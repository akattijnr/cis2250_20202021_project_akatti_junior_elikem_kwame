package info.hccis.cis2250.flowershop.ui.about;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AboutViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public AboutViewModel() {
        mText = new MutableLiveData<>();
        //mText.setValue("This is feedback fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}