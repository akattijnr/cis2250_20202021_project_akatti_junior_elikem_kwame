package info.hccis.cis2250.flowershop.ui.signin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import info.hccis.cis2250.flowershop.MainActivity;
import info.hccis.cis2250.flowershop.R;

public class SignInActivity extends AppCompatActivity {

    //Declaring global variables
    private SignInButton signInButton;
    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 0;
    //private TextView tvFailureMsg = (TextView) "Signed in not successfully, please try again.";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        signInButton = findViewById(R.id.sign_in_button);

        /*
         * Method used to invoke the signIn() method
         * https://developers.google.com/identity/sign-in/android/start-integrating
         * Author: Elikem K. Akatti Jnr
         * Date: 2021/02/05
         * Purpose: Used for google sign in presentation
         */
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.sign_in_button:
                        signIn();
                        break;

                }
            }
        });

        // Configure sign-in to request the user's ID, email textview_detail_address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }


    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            Intent intent = new Intent(SignInActivity.this, MainActivity.class);
            startActivity(intent);
            Toast.makeText(SignInActivity.this, "Signed in successfully", Toast.LENGTH_LONG).show();
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Error","signInResult:failed code=" + e.getStatusCode());

            //Show appropriate message if sign in not successfully
            //Toast.makeText(SignInActivity.this, "Signed in not successfully, please try again.", Toast.LENGTH_LONG). show();

            Toast toast = Toast.makeText(SignInActivity.this, "Signed in not successfully, please try again.", Toast.LENGTH_LONG);
            View view = toast.getView();
            view.setBackgroundColor(Color.parseColor("#F6AE2D"));
            toast.show();

            //view.getBackground().setColorFilter("white", PorterDuff.Mode.SRC_IN);



           // TextView toastMessage = (TextView) toast.setText("Signed in not successfully, please try again."). getView().findViewById(android.R.id.message);
            //toastMessage.setTextColor(Color.RED);
            //toast.show();
        }
    }
}

