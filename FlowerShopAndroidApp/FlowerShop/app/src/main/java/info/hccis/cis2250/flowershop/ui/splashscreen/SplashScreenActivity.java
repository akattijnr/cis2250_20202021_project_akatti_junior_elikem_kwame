package info.hccis.cis2250.flowershop.ui.splashscreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import info.hccis.cis2250.flowershop.MainActivity;
import info.hccis.cis2250.flowershop.R;
import info.hccis.cis2250.flowershop.ui.signin.SignInActivity;

public class SplashScreenActivity extends AppCompatActivity {

    private TextView tv;
    private ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        //tv = findViewById(R.id.tv);
        iv = findViewById(R.id.iv);

        Animation myanim = AnimationUtils.loadAnimation(this, R.anim.mytransition);
        //tv.startAnimation(myanim);
        iv.startAnimation(myanim);
        Intent i = new Intent(this, SignInActivity.class);
        Thread timer = new Thread(){
            public void run () {
                try{
                    sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    startActivity(i);
                    finish();
                }
            }

        };
            timer.start();
    }
}