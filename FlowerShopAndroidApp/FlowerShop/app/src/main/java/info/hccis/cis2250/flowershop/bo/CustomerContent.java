package info.hccis.cis2250.flowershop.bo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import info.hccis.cis2250.flowershop.MainActivity;
import info.hccis.cis2250.flowershop.R;
import info.hccis.cis2250.flowershop.api.JsonCustomerApi;
import info.hccis.cis2250.flowershop.ui.customers.CustomersFragment;
import info.hccis.cis2250.flowershop.util.NotificationUtil;
import info.hccis.cis2250.flowershop.util.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class CustomerContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Customer> CUSTOMERS = new ArrayList<Customer>();

    /**
     * Reload the Room db with the provided list.
     *
     * @param customers
     * @author Elikem K. Akatti Jnr
     * <p>
     * todo:  This should be on a background thread.
     * @since 20210226
     */

    public static void reloadCustomersInRoom(List<Customer> customers) {
        //first delete all
        MainActivity.myAppDatabase.customerDAO().deleteAll();
        for (Customer current : customers) {
            MainActivity.myAppDatabase.customerDAO().add(current);
        }
        Log.d("EAKJ get from db", "Loading from room.");

    }


    /**
     * Get the list of campers that are currently in the Room database.
     *
     * @return the customers
     * @author Elikem K. Akatti Jnr
     * @since 20210226
     */

    public static List<Customer> getCustomersFromRoom() {

        Log.d("bjm get from db", "Loading from room.");


        List<Customer> customersBack = MainActivity.myAppDatabase.customerDAO().get();
        Log.d("EKAJ", "loaded from room db, size=" + customersBack.size());
                for (Customer current : customersBack) {
            Log.d("EKAJ", current.toString());
        }
        Log.d("EKAJ", "that's it");


        return customersBack;

    }



    /**
     * Load the campers.  This method will use the rest service to provide the data.  The reason it is
     * in this class is because it is changing the value to the FLOWER_SHOPS list.  This is the list which
     * is used to back the RecyclerView.
     *
     * @author BJM taken from Alex/Thomas' presentation.
     * @since 20200116
     */

    public static void loadCustomers(Activity context) {


        Log.d("BJM", "Accessing api at:" + Util.CUSTOMER_BASE_API);

        //Use Retrofit to connect to the service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Util.CUSTOMER_BASE_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonCustomerApi jsonCustomerApi = retrofit.create(JsonCustomerApi.class);

        //Create a list of campers.
        Call<List<Customer>> call = jsonCustomerApi.getCustomers();

        //final reference to activity to get shared preferences
        final Activity contextIn = context;

        call.enqueue(new Callback<List<Customer>>() {

            @Override
            public void onResponse(Call<List<Customer>> call, Response<List<Customer>> response) {

                List<Customer> customers;

                if(!response.isSuccessful()) {
                    Log.d("EAKJ rest", "not successful response from rest for customers Code=" + response.code());
                    SharedPreferences sharedPref = contextIn.getPreferences(Context.MODE_PRIVATE);

                    boolean preferToLoad = sharedPref.getBoolean(contextIn.getString(R.string.preference_load_from_room), false);
                    if(preferToLoad){
                        customers = getCustomersFromRoom();
                        NotificationUtil.sendNotification("Customers loaded", "Customers loaded from room  (" + customers.size() + ")");

                    }else {
                        NotificationUtil.sendNotification("Customers not loaded", "Error connecting - Not loaded");
                        return;
                    }
                } else{

                    customers = response.body();

                    NotificationUtil.sendNotification("Customers loaded", "Customers loaded from service  (" + customers.size() + ")");

                    //Have successfully connected.  Reload the camper database.
                    reloadCustomersInRoom(customers);
                }

                Log.d("EAKJ", "data back from service call #returned=" + customers.size());

                //**********************************************************************************
                // Now that we have the customers, will use them to assign values to the list which
                // is backing the recycler view.
                //**********************************************************************************

                CustomerContent.CUSTOMERS.clear();
                CustomerContent.CUSTOMERS.addAll(customers);

                //**********************************************************************************
                // The CamperListFragment has a recyclerview which is used to show the camper list.
                // This recyclerview is backed by the camper list in the CustomerContent class.  After
                // this list is loaded, need to notify the adapter from the recyclerview that the
                // data is changed.
                //**********************************************************************************

                CustomersFragment.getRecyclerView().getAdapter().notifyDataSetChanged();

                //Remove the progress bar from the view.
                CustomersFragment.clearProgressBarVisitiblity();

            }

            @Override
            public void onFailure(Call<List<Customer>> call, Throwable t) {

                //**********************************************************************************
                // If the api call failed, give a notification to the user.
                //**********************************************************************************
                Log.d("EAKJ", "api call failed");
                Log.d("EAKJ", t.getMessage());

                SharedPreferences sharedPref = contextIn.getPreferences(Context.MODE_PRIVATE);

                //BJM 20200207 If the user wants to load from room...do so
                boolean preferToLoad = sharedPref.getBoolean(contextIn.getString(R.string.preference_load_from_room), false);

                CustomerContent.CUSTOMERS.clear();
                if (preferToLoad){
                    CustomerContent.CUSTOMERS.addAll(getCustomersFromRoom());

                    //Remove the progress bar from the view.
                    NotificationUtil.sendNotification("Customers loaded", "Customers loaded from room(" + CustomerContent.CUSTOMERS.size() + ")");

                }else {

                    CustomerContent.CUSTOMERS.clear();
                    //Remove the progress bar from the view.
                    NotificationUtil.sendNotification("Customers not loaded", "Error connecting - Not loaded");
                }

                CustomersFragment.getRecyclerView().getAdapter().notifyDataSetChanged();
                //Remove the progress bar from the view.
                CustomersFragment.clearProgressBarVisitiblity();

            }
        });
    }
}