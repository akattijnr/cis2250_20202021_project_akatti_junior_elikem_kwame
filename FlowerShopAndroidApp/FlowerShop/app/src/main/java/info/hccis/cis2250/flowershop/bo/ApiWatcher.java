package info.hccis.cis2250.flowershop.bo;

import android.app.Activity;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import info.hccis.cis2250.flowershop.api.JsonCustomerApi;
import info.hccis.cis2250.flowershop.ui.customers.CustomersFragment;
import info.hccis.cis2250.flowershop.util.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiWatcher extends Thread {

    private int lengthLastCall = -1;  //Number of rows returned

    //The activity is passed in to allow the runOnUIThread to be used.
    private Activity activity = null;

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void run() {
        super.run();
        try {

            do {

                Thread.sleep(10000); //Check api every 10 seconds

                //************************************************************************
                // A lot of this code is duplicated from the CustomerContent class.  It will
                // access the api and if if notes that the number of campers has changed, will
                // notify the CampersFragment that the data is changed.
                //************************************************************************

                //Use Retrofit to connect to the service
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Util.CUSTOMER_BASE_API)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                JsonCustomerApi jsonCustomerApi = retrofit.create(JsonCustomerApi.class);

                //Create a list of campers.
                Call<List<Customer>> call = jsonCustomerApi.getCustomers();

                call.enqueue(new Callback<List<Customer>>() {

                    @Override
                    public void onResponse(Call<List<Customer>> call, Response<List<Customer>> response) {

                        List<Customer> customers = new ArrayList();

                        if (!response.isSuccessful()) {
                            Log.d("eakj rest", "not successful response from rest for campers Code=" + response.code());

                        } else {

                            customers = response.body();
                            int lengthThisCall = customers.size();

                            if (lengthLastCall == -1) {
                                //first time - don't notify
                                lengthLastCall = lengthThisCall;
                            } else if (lengthThisCall != lengthLastCall) {
                                //data has changed
                                Log.d("EAKJ background", "Data has changed");
                                lengthLastCall = lengthThisCall;

                                //**********************************************************************
                                // This method will allow a call to the runOnUiThread which will be allowed
                                // to interact with the ui components of the app.
                                //**********************************************************************
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        CustomersFragment.notifyDataChanged("Data change detected.");
                                    }

                                });

                            } else {
                                Log.d("EAKJ background", "Data has NOT changed");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Customer>> call, Throwable t) {

                        //**********************************************************************************
                        // If the api call failed, give a notification to the user.
                        //**********************************************************************************
                        Log.d("eakj", "api call failed");
                        Log.d("eakj", t.getMessage());
                    }
                });

            } while (true);
        } catch (InterruptedException e) {
            Log.d("EAKJ","Thread interupted.  Stopping in the thread.");
        }
    }
}
