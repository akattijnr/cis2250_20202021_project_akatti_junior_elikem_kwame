package info.hccis.cis2250.flowershop.bo;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

import kotlin.jvm.Transient;


/**
 *
 * @author Elikem
 */

@Entity(tableName = "customer")


public class Customer implements Serializable
{


    private static final long serialVersionUID = 1L;

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private Integer id;

    @ColumnInfo(name = "customerTypeId")
    private Integer customerTypeId;
    @ColumnInfo(name = "full_name")
    private String fullName;
    @ColumnInfo(name = "address1")
    private String address1;
    @ColumnInfo(name = "city")
    private String city;
    @ColumnInfo(name = "province")
    private String province;
    @ColumnInfo(name = "postalCode")
    private String postalCode;
    @ColumnInfo(name = "phoneNumber")
    private String phoneNumber;
    @ColumnInfo(name = "birthDate")
    private String birthDate;
    @ColumnInfo(name = "loyaltyCard")
    private String loyaltyCard;
    @ColumnInfo(name = "customerTypeDescription")
    private String customerTypeDescription;

    public String getCustomerTypeDescription() {
        return customerTypeDescription;
    }

    public void setCustomerTypeDescription(String customerTypeDescription) {
        this.customerTypeDescription = customerTypeDescription;
    }
    
    public Customer()
    {
    }

    public Customer(Integer id)
    {
        this.id = id;
    }

    public Customer(Integer id, String fullName)
    {
        this.id = id;
        this.fullName = fullName;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getCustomerTypeId()
    {
        return customerTypeId;
    }

    public void setCustomerTypeId(Integer customerTypeId)
    {
        this.customerTypeId = customerTypeId;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getAddress1()
    {
        return address1;
    }

    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getProvince()
    {
        return province;
    }

    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(String birthDate)
    {
        this.birthDate = birthDate;
    }

    public String getLoyaltyCard()
    {
        return loyaltyCard;
    }

    public void setLoyaltyCard(String loyaltyCard)
    {
        this.loyaltyCard = loyaltyCard;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customer)) {
            return false;
        }
        Customer other = (Customer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "info.hccis.flowershop.entity.jpa.Customer[ id=" + id + " ]";
    }
    
}
