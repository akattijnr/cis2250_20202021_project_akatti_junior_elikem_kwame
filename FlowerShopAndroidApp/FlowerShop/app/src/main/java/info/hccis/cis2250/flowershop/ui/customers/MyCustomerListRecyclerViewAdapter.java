package info.hccis.cis2250.flowershop.ui.customers;

import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import info.hccis.cis2250.flowershop.R;
import info.hccis.cis2250.flowershop.bo.Customer;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Customer} and makes a call to the
 * TODO: Replace the implementation with code for your data type.
 */
public class MyCustomerListRecyclerViewAdapter extends RecyclerView.Adapter<MyCustomerListRecyclerViewAdapter.ViewHolder> {

    private final List<Customer> mValues;
    private final CustomersFragment.OnListFragmentInteractionListener mListener;

    public MyCustomerListRecyclerViewAdapter(List<Customer> items, CustomersFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_customer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        holder.mFullNameView.setText(mValues.get(position).getFullName());
        holder.mDobView.setText(mValues.get(position).getBirthDate());



        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("EAKJ", "clicked on a row of the recyclerview.  ");


                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public final TextView mFullNameView;
        public final TextView mDobView;

        public Customer mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            mDobView = (TextView) view.findViewById(R.id.dob);
            mFullNameView = (TextView) view.findViewById(R.id.full_name);
        }

        @Override
        public String toString() {
            return super.toString() + "   '" + mFullNameView.toString() + "'"
                    + " '"+mDobView.toString()+ System.getProperty("line.separator");
        }
    }
}
